const nodemailer = require('nodemailer');
const mailGun = require('nodemailer-mailgun-transport');

const auth = {
  auth: {
    api_key: 'key-90829baff80154f6ed8a4e7204fb4952',
    domain: 'sandboxe0f4d256a4344e38825a68b120112ab7.mailgun.org'
  }
};

const transport = nodemailer.createTransport(mailGun(auth));

const sendMail = (subject, email, text, cb) => {
  const mailOption = {
    from: email,
    to: 'contatopedrorn@gmail.com',
    subject: subject,
    text: text
  };

  transport.sendMail(mailOption, (err, data) => {
    if (err) {
      cb(err, null);
    } else {
      cb(err, data);
    }
  });
};

module.exports = sendMail;
